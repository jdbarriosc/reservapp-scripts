import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:reservapp_scripts/routes.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ROUTES initialRoute;
    try {
      FirebaseAuth.instance.currentUser.email;
      initialRoute = ROUTES.home;
    } catch (error) {
      initialRoute = ROUTES.login;
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'ReservApp Scripts',
      initialRoute: initialRoute.toString(),
      routes: routes,
      theme: ThemeData(
        primaryColor: Colors.grey,
      ),
    );
  }
}
