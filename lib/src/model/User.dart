import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  int id;
  String name;
  String email;
  String cellPhone;
  String tipoDoc;
  List allergies;

  User({
    this.id,
    this.name,
    this.email,
    this.cellPhone,
    this.tipoDoc,
    this.allergies,
  });
}
