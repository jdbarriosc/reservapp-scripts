import 'package:cloud_firestore/cloud_firestore.dart';

class UserActionStatistics {
  String userId;
  Map<String, int> restaurantIdStatistics = {};
  Map<String, int> restaurantNameStatistics = {};
  Map<String, int> restaurantAddressStatistics = {};
  Map<String, int> restaurantCategoryStatistics = {};

  String favoriteRestaurantId;
  String favoriteRestaurantName;
  String favoriteRestaurantAddress;
  String favoriteRestaurantCategory;

  int favoriteRestaurantIdCount = 0;
  int favoriteRestaurantNameCount = 0;
  int favoriteRestaurantAddressCount = 0;
  int favoriteRestaurantCategoryCount = 0;

  void processRestaurantId(String restaurantId) {
    if (restaurantIdStatistics[restaurantId] == null) {
      restaurantIdStatistics[restaurantId] = 1;
    } else {
      restaurantIdStatistics[restaurantId] += 1;
    }

    if (restaurantIdStatistics[restaurantId] > favoriteRestaurantIdCount) {
      favoriteRestaurantId = restaurantId;
      favoriteRestaurantIdCount = restaurantIdStatistics[restaurantId];
    }
  }

  void processRestaurantName(String restaurantName) {
    if (restaurantNameStatistics[restaurantName] == null) {
      restaurantNameStatistics[restaurantName] = 1;
    } else {
      restaurantNameStatistics[restaurantName] += 1;
    }

    if (restaurantNameStatistics[restaurantName] >
        favoriteRestaurantNameCount) {
      favoriteRestaurantName = restaurantName;
      favoriteRestaurantNameCount = restaurantNameStatistics[restaurantName];
    }
  }

  void processRestaurantAddress(String restaurantAddress) {
    if (restaurantAddressStatistics[restaurantAddress] == null) {
      restaurantAddressStatistics[restaurantAddress] = 1;
    } else {
      restaurantAddressStatistics[restaurantAddress] += 1;
    }

    if (restaurantAddressStatistics[restaurantAddress] >
        favoriteRestaurantAddressCount) {
      favoriteRestaurantAddress = restaurantAddress;
      favoriteRestaurantAddressCount =
          restaurantAddressStatistics[restaurantAddress];
    }
  }

  void processRestaurantCategory(String restaurantCategory) {
    if (restaurantCategoryStatistics[restaurantCategory] == null) {
      restaurantCategoryStatistics[restaurantCategory] = 1;
    } else {
      restaurantCategoryStatistics[restaurantCategory] += 1;
    }

    if (restaurantCategoryStatistics[restaurantCategory] >
        favoriteRestaurantCategoryCount) {
      favoriteRestaurantCategory = restaurantCategory;
      favoriteRestaurantCategoryCount =
          restaurantCategoryStatistics[restaurantCategory];
    }
  }

  UserActionStatistics(
    String pUserId,
    List<QueryDocumentSnapshot> userActionDocs,
  ) {
    userId = pUserId;
    userActionDocs.forEach((userActionDoc) {
      final String restaurantId = userActionDoc['restaurantId'];
      final String restaurantName = userActionDoc['restaurantName'];
      final String restaurantAddress = userActionDoc['restaurantAddress'];
      final String restaurantCategory = userActionDoc['restaurantCategory'];
      if (restaurantId != null) {
        processRestaurantId(restaurantId);
      }
      if (restaurantName != null) {
        processRestaurantName(restaurantName);
      }
      if (restaurantAddress != null) {
        processRestaurantAddress(restaurantAddress);
      }
      if (restaurantCategory != null) {
        processRestaurantCategory(restaurantCategory);
      }
    });
  }

  String toString() => '''
      favoriteRestaurantId: $favoriteRestaurantId ($favoriteRestaurantIdCount)\n
      favoriteRestaurantName: $favoriteRestaurantName ($favoriteRestaurantNameCount)\n
      favoriteRestaurantAddress: $favoriteRestaurantAddress ($favoriteRestaurantAddressCount)\n
      favoriteRestaurantCategory: $favoriteRestaurantCategory ($favoriteRestaurantCategoryCount)\n
      ${restaurantIdStatistics.toString()}\n
      ${restaurantNameStatistics.toString()}\n
      ${restaurantAddressStatistics.toString()}\n
      ${restaurantCategoryStatistics.toString()}\n
      ''';

  Map<String, dynamic> toFirebaseDoc() => {
        'userId': userId,
        'restaurantIdStatistics': restaurantIdStatistics,
        'restaurantNameStatistics': restaurantNameStatistics,
        'restaurantAddressStatistics': restaurantAddressStatistics,
        'restaurantCategoryStatistics': restaurantCategoryStatistics,
        'favoriteRestaurantId': favoriteRestaurantId,
        'favoriteRestaurantName': favoriteRestaurantName,
        'favoriteRestaurantAddress': favoriteRestaurantAddress,
        'favoriteRestaurantCategory': favoriteRestaurantCategory,
        'favoriteRestaurantIdCount': favoriteRestaurantIdCount,
        'favoriteRestaurantNameCount': favoriteRestaurantNameCount,
        'favoriteRestaurantAddressCount': favoriteRestaurantAddressCount,
        'favoriteRestaurantCategoryCount': favoriteRestaurantCategoryCount,
      };
}
