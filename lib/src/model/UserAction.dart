import 'package:firebase_auth/firebase_auth.dart';

final String userIdKey = 'userId';
final String restaurantIdKey = 'restaurantId';
final String restaurantNameKey = 'restaurantName';
final String restaurantAddressKey = 'restaurantAddress';
final String restaurantCategoryKey = 'restaurantCategory';
final String dateKey = 'date';
final String hadConnectionKey = 'hadConnection';
final String typeKey = 'type';
final String viewKey = 'view';

enum USER_ACTION_TYPES {
  viewMenu,
  viewCategory,
  viewEditBookingForm,
  viewBookingForm,
  saveBooking,
}

enum VIEWS {
  allergies,
  bookings,
  bookingsDialog,
  home,
  login,
  profile,
  restaurants,
  restaurantsDialog,
  signup,
  qrCode,
  notification,
}

class UserAction {
  String userId;
  DateTime date;
  final String restaurantId;
  final String restaurantName;
  final String restaurantAddress;
  final String restaurantCategory;
  final bool hadConnection;
  String _type;
  String _view;

  UserAction({
    this.restaurantId,
    this.restaurantName,
    this.restaurantAddress,
    this.restaurantCategory,
    this.hadConnection: true,
    USER_ACTION_TYPES type,
    VIEWS view,
  }) {
    userId = FirebaseAuth.instance.currentUser.email;
    date = DateTime.now();
    _type = type.toString();
    _view = view.toString();
  }

  Map<String, dynamic> toFirebaseDoc() => {
        userIdKey: userId,
        restaurantIdKey: restaurantId,
        restaurantNameKey: restaurantName,
        restaurantAddressKey: restaurantAddress,
        restaurantCategoryKey: restaurantCategory,
        dateKey: date,
        hadConnectionKey: hadConnection,
        typeKey: _type,
        viewKey: _view,
      };

  Map<String, Object> toSQLRow() => {
        userIdKey: userId,
        restaurantIdKey: restaurantId,
        restaurantNameKey: restaurantName,
        restaurantAddressKey: restaurantAddress,
        restaurantCategoryKey: restaurantCategory,
        dateKey: date.toIso8601String(),
        hadConnectionKey: hadConnection,
        typeKey: _type,
        viewKey: _view,
      };
}
