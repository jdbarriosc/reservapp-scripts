import 'package:cloud_firestore/cloud_firestore.dart';

final String bookingTableName = 'bookings';

final String imageKey = 'image';
final String restaurantAddressKey = 'restaurantAddress';
final String restaurantNameKey = 'restaurantName';
final String restaurantIdKey = 'restaurantId';
final String restaurantCategoryKey = 'restaurantCategory';
final String userIdKey = 'userId';
final String dateKey = 'date';
final String peopleQuantityKey = 'peopleQuantity';

class Booking {
  String id;
  DateTime date;
  int peopleQuantity;
  final String image;
  final String userId;
  final String restaurantAddress;
  final String restaurantName;
  final String restaurantId;
  final String restaurantCategory;

  Booking({
    this.id,
    this.date,
    this.peopleQuantity,
    this.image,
    this.userId,
    this.restaurantId,
    this.restaurantName,
    this.restaurantAddress,
    this.restaurantCategory,
  });

  factory Booking.fromFirestore(QueryDocumentSnapshot bookingDoc) {
    return Booking(
      id: bookingDoc.reference.id,
      date: bookingDoc['date'].toDate(),
      peopleQuantity: bookingDoc['peopleQuantity'],
      image: bookingDoc['image'],
      userId: bookingDoc['userId'],
      restaurantId: bookingDoc['restaurantId'],
      restaurantName: bookingDoc['restaurantName'],
      restaurantAddress: bookingDoc['restaurantAddress'],
      // restaurantCategory: bookingDoc['restaurantCategory'],
    );
  }

  Map<String, dynamic> toFirebaseDoc() => {
        dateKey: date,
        peopleQuantityKey: peopleQuantity,
        imageKey: image,
        userIdKey: userId,
        restaurantIdKey: restaurantId,
        restaurantNameKey: restaurantName,
        restaurantAddressKey: restaurantAddress,
        restaurantCategoryKey: restaurantCategory,
      };

  static List<Booking> makeBookingListFromQuerySnapshot(
    QuerySnapshot bookingsQuerySnapshot,
  ) {
    List<Booking> bookings = bookingsQuerySnapshot.docs.map(
      (bookingDoc) => Booking.fromFirestore(
        bookingDoc,
      ),
    );
    return bookings;
  }
}
