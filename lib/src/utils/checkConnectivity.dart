import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

Future<bool> checkConnectivity(BuildContext context) async {
  final connectivityResult = await (Connectivity().checkConnectivity());

  if (connectivityResult == ConnectivityResult.none) {
    showTopSnackBar(
      context,
      CustomSnackBar.info(
        message: 'No connection available',
      ),
    );
    return false;
  }

  return true;
}
