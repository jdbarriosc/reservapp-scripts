import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:reservapp_scripts/src/utils/Validators.dart';
import 'package:reservapp_scripts/src/utils/checkConnectivity.dart';
import 'package:rxdart/rxdart.dart';

class UserProvider with Validators {
  final _emailController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();
  final _tipoDocController = BehaviorSubject<String>();
  final _idController = BehaviorSubject<String>();
  final _nameController = BehaviorSubject<String>();
  final _phoneController = BehaviorSubject<String>();

  // Recuperate data from stream
  Stream<String> get emailStream =>
      _emailController.stream.transform(validarEmail);
  Stream<String> get passwordStream =>
      _passwordController.stream.transform(validarPassword);
  Stream<String> get tipoDocStream => _tipoDocController.stream;
  Stream<String> get idStream => _idController.stream;
  Stream<String> get nameStream => _nameController.stream;
  Stream<String> get phoneStream => _phoneController.stream;

  Stream<bool> get formValidStream =>
      Observable.combineLatest2(emailStream, passwordStream, (e, p) => true);

  // Insert values to stream
  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;
  Function(String) get changeTipoDoc => _tipoDocController.sink.add;
  Function(String) get changeId => _idController.sink.add;
  Function(String) get changeName => _nameController.sink.add;
  Function(String) get changePhone => _phoneController.sink.add;

  // Get last value inserted to stream
  String get email => _emailController.value;
  String get password => _passwordController.value;
  String get tipoDoc => _tipoDocController.value;
  String get id => _idController.value;
  String get name => _nameController.value;
  String get phone => _phoneController.value;

  static Future<List<String>> getUserIds(
    BuildContext context,
  ) async {
    if (await checkConnectivity(context)) {
      CollectionReference usersCollection =
          FirebaseFirestore.instance.collection('users');
      final usersQuerySnapshot = await usersCollection.get();

      final List<String> userIds = [];
      usersQuerySnapshot.docs.forEach(
        (userDoc) => userIds.add(
          userDoc.reference.id,
        ),
      );

      return userIds;
    } else {
      return null;
    }
  }

  dispose() {
    _emailController?.close();
    _passwordController?.close();
    _tipoDocController?.close();
    _idController?.close();
    _nameController?.close();
    _phoneController?.close();
  }
}
