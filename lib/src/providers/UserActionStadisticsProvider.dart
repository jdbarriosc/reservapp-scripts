import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:reservapp_scripts/src/model/UserActionStatistics.dart';
import 'package:reservapp_scripts/src/utils/checkConnectivity.dart';

class UserActionStadisticsProvider {
  static void calculateUserActionStatistics(
    BuildContext context,
    String userId,
  ) async {
    UserActionStatistics userActionStatistics;

    if (await checkConnectivity(context)) {
      final CollectionReference userActionsCollection =
          FirebaseFirestore.instance.collection('userActions');

      final getUserActionsQuerySnapshot = await userActionsCollection
          .where(
            'userId',
            isEqualTo: userId,
          )
          .get();

      userActionStatistics = UserActionStatistics(
        userId,
        getUserActionsQuerySnapshot.docs,
      );

      UserActionStadisticsProvider.postUserActionStadistics(
        context,
        userActionStatistics,
      );
    }
  }

  static void postUserActionStadistics(
    BuildContext context,
    UserActionStatistics userActionStatistics,
  ) async {
    if (await checkConnectivity(context)) {
      final CollectionReference userActionsCollection =
          FirebaseFirestore.instance.collection('userActionStadistics');

      final userActionStadisticsQuerySnapshot = await userActionsCollection
          .where(
            'userId',
            isEqualTo: userActionStatistics.userId,
          )
          .get();

      if (userActionStadisticsQuerySnapshot.docs.length > 0) {
        final currentUserActionStadistics =
            userActionStadisticsQuerySnapshot.docs[0];

        await userActionsCollection
            .doc(
              currentUserActionStadistics.reference.id,
            )
            .update(
              userActionStatistics.toFirebaseDoc(),
            );
      } else {
        await userActionsCollection.add(
          userActionStatistics.toFirebaseDoc(),
        );
      }
    }
  }
}
