import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

List<Map<String, dynamic>> locations = [
  {
    'latitude': 4.698646,
    'longitude': -74.0443626,
    'neighborhood': 'Santa Bárbara',
    'address': 'Street 119 #15a-43',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.672155,
    'longitude': -74.0567088,
    'neighborhood': 'Virrey',
    'address': 'Street 86 # 18 - 24',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.69256,
    'longitude': -74.048675,
    'neighborhood': 'Chicó Navarra',
    'address': 'Avenue 18 con 102',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.676766,
    'longitude': -74.049255,
    'neighborhood': 'Chicó Norte II',
    'address': 'Street 97a # 8a-45',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67672134557744,
    'longitude': -74.0416936995952,
    'neighborhood': 'La Gran Vía',
    'address': 'Avenue 8 con 95',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.692762,
    'longitude': -74.043034,
    'neighborhood': 'Santa Paula',
    'address': 'Avenue 11 con 111',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.67553336418364,
    'longitude': -74.0513876802925,
    'neighborhood': 'Chicó',
    'address': 'Street 92 #14-23',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.65772280891627,
    'longitude': -74.0511926109265,
    'neighborhood': 'Rosales',
    'address': 'Avenue 4 con 76',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.66925353470931,
    'longitude': -74.0537220542426,
    'neighborhood': 'Virrey',
    'address': 'Avenue 13 # 85',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.65153421177485,
    'longitude': -74.0502632004242,
    'neighborhood': 'Rosales',
    'address': 'Avenue 1°#71',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.661233,
    'longitude': -74.04861,
    'neighborhood': 'Rosales',
    'address': 'Avenue 1°#78',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.672526,
    'longitude': -74.047657,
    'neighborhood': 'La Cabrera',
    'address': 'Avenue 10 #91-11',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.6938687887914,
    'longitude': -74.0595154577971,
    'neighborhood': 'Pasadena',
    'address': 'Avenue 49b # 105',
    'zone': 'Córdoba'
  },
  {
    'latitude': 4.676181,
    'longitude': -74.0508879,
    'neighborhood': 'Chicó',
    'address': 'Avenue 14 #92-51',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.707244,
    'longitude': -74.047426,
    'neighborhood': 'La Streetja',
    'address': 'Street 128 con 20',
    'zone': 'La Carolina'
  },
  {
    'latitude': 4.680211,
    'longitude': -74.047062,
    'neighborhood': 'Chicó Norte',
    'address': 'Avenue 9 bis #97-19',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.63153308457496,
    'longitude': -74.0759526219897,
    'neighborhood': 'La Soledad',
    'address': 'Avenue 25 #41-22',
    'zone': 'Teusaquillo'
  },
  {
    'latitude': 4.661233,
    'longitude': -74.04861,
    'neighborhood': 'Rosales',
    'address': 'Tranv 1 East no 54a-04',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67430764099854,
    'longitude': -74.0478095623657,
    'neighborhood': 'Chicó Norte II',
    'address': 'Avenue. 11 # 93A-86',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.6772231550000924,
    'longitude': -74.05267441699995,
    'neighborhood': 'Chicó Norte',
    'address': 'Street 93 con 16',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.68076273201152,
    'longitude': -74.0527528146875,
    'neighborhood': 'Chicó Norte III',
    'address': 'Street. 94 # 18-50/ 54',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.641213,
    'longitude': -74.057467,
    'neighborhood': 'El Castillo',
    'address': 'Tranv 1 East no 54a-04',
    'zone': 'Chapinero'
  },
  {
    'latitude': 4.684753,
    'longitude': -74.0441085,
    'neighborhood': 'Rincón del Chicó',
    'address': 'Avenue 12 # 101a - 18',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.65593189853281,
    'longitude': -74.0491298432122,
    'neighborhood': 'Rosales',
    'address': 'Street 77 # 1',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.672916,
    'longitude': -74.0559276,
    'neighborhood': 'Virrey',
    'address': 'Avenue 18 # 86a - 69',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67292329378984,
    'longitude': -74.0454410324872,
    'neighborhood': 'Chicó Norte II',
    'address': 'Street. 93b # 9-39',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.683633,
    'longitude': -74.055048,
    'neighborhood': 'Chicó Norte III',
    'address': 'Street 94',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.6888668455841,
    'longitude': -74.0425055106102,
    'neighborhood': 'Santa Paula',
    'address': 'Street 106 #13-45',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.696194,
    'longitude': -74.0463947,
    'neighborhood': 'San Patricio',
    'address': 'Avenue 17 # 113 - 44 ',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.676286,
    'longitude': -74.0577043,
    'neighborhood': 'Virrey',
    'address': 'Avenue 19c# 89-12',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67608940447411,
    'longitude': -74.0437992140245,
    'neighborhood': 'Chicó Reservado',
    'address': 'Street 94a # 9 - 5',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.676766,
    'longitude': -74.049255,
    'neighborhood': 'Chicó Norte II',
    'address': 'Avenida Avenue 11 #96-59',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67355,
    'longitude': -74.05631,
    'neighborhood': 'Virrey',
    'address': 'Avenue. 19A # 86A-70',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.701701,
    'longitude': -74.0352963,
    'neighborhood': 'Santa Bárbara',
    'address': 'Avenue 11b#123-70',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.68233312861246,
    'longitude': -74.0508416839174,
    'neighborhood': 'Chicó',
    'address': 'Street 96 #17-39',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67206244802026,
    'longitude': -74.0564081488677,
    'neighborhood': 'Virrey',
    'address': 'Avenue 18# 86 A 6',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.66153592801511,
    'longitude': -74.0484023132477,
    'neighborhood': 'Rosales',
    'address': 'Avenue 5 # 81-30',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.682326,
    'longitude': -74.049224,
    'neighborhood': 'Chicó Norte',
    'address': 'Street 97 # 21A -50',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.699993,
    'longitude': -74.048817,
    'neighborhood': 'Santa Bárbara',
    'address': 'Avenue 18c #118-79',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.678289,
    'longitude': -74.0562573,
    'neighborhood': 'Virrey',
    'address': 'Avenue 19b # 92-22',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.68972119120533,
    'longitude': -74.0442369607672,
    'neighborhood': 'San Patricio',
    'address': 'Avenue 14b #106-60',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.661858,
    'longitude': -74.0488896,
    'neighborhood': 'Rosales',
    'address': 'Avenue 6 #80a-33',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.68083,
    'longitude': -74.0513048,
    'neighborhood': 'Chicó Norte',
    'address': 'Street 94a #16-29',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.661233,
    'longitude': -74.04861,
    'neighborhood': 'Rosales',
    'address': 'Trv 3 con 84',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.69276635852997,
    'longitude': -74.0507618330916,
    'neighborhood': 'San Patricio',
    'address': 'Avenue 19a #106a-41/61',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.63922704093677,
    'longitude': -74.0607479258257,
    'neighborhood': 'Chapinero Alto',
    'address': 'Avenue 4a con 54',
    'zone': 'Chapinero'
  },
  {
    'latitude': 4.69279327644783,
    'longitude': -74.0550246890169,
    'neighborhood': 'Chicó Navarra',
    'address': 'Street 105 # 23 - 36',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.65153421177485,
    'longitude': -74.0502632004242,
    'neighborhood': 'Rosales',
    'address': 'Avenue 1 East #71-24',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.640498,
    'longitude': -74.061058,
    'neighborhood': 'Chapinero Alto',
    'address': 'Avenue 2 La salle',
    'zone': 'Chapinero'
  },
  {
    'latitude': 4.66873134415067,
    'longitude': -74.0585473064992,
    'neighborhood': 'Lago Gaitán',
    'address': 'Street 82 # 19a-29',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.661233,
    'longitude': -74.04861,
    'neighborhood': 'Rosales',
    'address': 'Transversal 2 East # 78-21',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.645334,
    'longitude': -74.054369,
    'neighborhood': 'Chapinero Alto',
    'address': 'Street 64 #1-15',
    'zone': 'Chapinero'
  },
  {
    'latitude': 4.69578847077704,
    'longitude': -74.0398794391948,
    'neighborhood': 'Santa Paula',
    'address': 'Street 116 #13b-35',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.684874,
    'longitude': -74.0458415,
    'neighborhood': 'Rincón del Chicó',
    'address': 'Avenue 13a #101-09',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.68410993191174,
    'longitude': -74.0522171207826,
    'neighborhood': 'Chicó Norte III',
    'address': 'Street 97 con 19',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.638558,
    'longitude': -74.0604478,
    'neighborhood': 'Chapinero Alto',
    'address': 'Street 53 #3-85',
    'zone': 'Chapinero'
  },
  {
    'latitude': 4.65887588871958,
    'longitude': -74.0501851036711,
    'neighborhood': 'Rosales',
    'address': 'Avenue 4 #78-45',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.66516928665637,
    'longitude': -74.0469806833662,
    'neighborhood': 'Nogal',
    'address': 'Street 85 Avenue 7',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67605710510693,
    'longitude': -74.052413571472,
    'neighborhood': 'Chicó',
    'address': 'Street 92 # 15',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.698216,
    'longitude': -74.053235,
    'neighborhood': 'Santa Bárbara Occidental',
    'address': 'Street 114a #21-72',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.677123,
    'longitude': -74.044137,
    'neighborhood': 'Chicó Reservado',
    'address': 'Avenue 11 #96-59',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.719884,
    'longitude': -74.0475835,
    'neighborhood': 'Nueva Autopista',
    'address': 'Street 135 #19-78',
    'zone': 'Usaquén'
  },
  {
    'latitude': 4.67403,
    'longitude': -74.048532,
    'neighborhood': 'Chicó Norte',
    'address': 'Street 92 #11-50',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67683209104759,
    'longitude': -74.0452627231394,
    'neighborhood': 'Chicó Reservado',
    'address': 'Avenue 10 con 94a',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67206244802026,
    'longitude': -74.0564081488677,
    'neighborhood': 'Virrey',
    'address': 'Avenue 18 #86a-69',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67292329378984,
    'longitude': -74.0454410324872,
    'neighborhood': 'Chicó',
    'address': 'Avenue 9 # 93b - 49',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.651513,
    'longitude': -74.050472,
    'neighborhood': 'Rosales',
    'address': 'Avenue 1 con 72',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.64049,
    'longitude': -74.072865,
    'neighborhood': 'Chapinero Alto',
    'address': 'Street 52#5',
    'zone': 'Chapinero'
  },
  {
    'latitude': 4.656964,
    'longitude': -74.048363,
    'neighborhood': 'Rosales',
    'address': 'Avenue 1 #77-05',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67202,
    'longitude': -74.055008,
    'neighborhood': 'Virrey',
    'address': 'Avenue 16 #86a-14',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.64007192671859,
    'longitude': -74.0699733776505,
    'neighborhood': 'Teusaquillo',
    'address': '52 con 18',
    'zone': 'Teusaquillo'
  },
  {
    'latitude': 4.69386527327513,
    'longitude': -74.040294637192,
    'neighborhood': 'Molinos del Norte',
    'address': 'Street 113 con 13',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.661233,
    'longitude': -74.04861,
    'neighborhood': 'Rosales',
    'address': 'Transversal 1°#68',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.678949,
    'longitude': -74.05184,
    'neighborhood': 'Chicó',
    'address': 'Avenue 19 # 91-58',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.64513416354296,
    'longitude': -74.0567907580072,
    'neighborhood': 'Chapinero Alto',
    'address': 'Street l 62 # 3',
    'zone': 'Chapinero'
  },
  {
    'latitude': 4.68084,
    'longitude': -74.0452468,
    'neighborhood': 'Chicó',
    'address': 'Avenue 11a #97-42',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.673281,
    'longitude': -74.058031,
    'neighborhood': 'Virrey',
    'address': 'Street 86a #19b-22',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.68777629159599,
    'longitude': -74.0515935807326,
    'neighborhood': 'Chicó Navarra',
    'address': 'Avenue 19a con 102',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.685312,
    'longitude': -74.044827,
    'neighborhood': 'Rincón del Chicó',
    'address': 'Avenue 13 # 101-81',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.689742,
    'longitude': -74.0555282,
    'neighborhood': 'Chicó Navarra',
    'address': 'Avenue 23 # 103-23',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.69587104040443,
    'longitude': -74.0519658814096,
    'neighborhood': 'San Patricio',
    'address': 'Street 109 #20-10',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.643319,
    'longitude': -74.0545265,
    'neighborhood': 'Chapinero Alto',
    'address': 'Transversal 4 East #61-05',
    'zone': 'Chapinero'
  },
  {
    'latitude': 4.678949,
    'longitude': -74.05184,
    'neighborhood': 'Chicó',
    'address': 'Avenue 8a con 96',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67608940447411,
    'longitude': -74.0437992140245,
    'neighborhood': 'Chicó Reservado',
    'address': 'Street 94a #9',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.6691626414404,
    'longitude': -74.0519908936315,
    'neighborhood': 'La Cabrera',
    'address': 'Street 86 con 15',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.67447499527892,
    'longitude': -74.042017251619,
    'neighborhood': 'Chicó Norte II',
    'address': 'Street 94a # 7a-26',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.651513,
    'longitude': -74.050472,
    'neighborhood': 'Rosales',
    'address': 'Street 72 con 1',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.699137,
    'longitude': -74.0523223,
    'neighborhood': 'Santa Bárbara',
    'address': 'Avenida. Street. 116 # 20-78',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.695129,
    'longitude': -74.0520925,
    'neighborhood': 'Chicó Navarra',
    'address': 'Avenue. 20 # 107-24',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.70063563712975,
    'longitude': -74.0348816493455,
    'neighborhood': 'Santa Bárbara',
    'address': 'Avenue. 11B # 123-30',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.645646,
    'longitude': -74.0547558,
    'neighborhood': 'Chapinero Alto',
    'address': 'Street 64 # 1-66',
    'zone': 'Chapinero'
  },
  {
    'latitude': 4.655941,
    'longitude': -74.0513506,
    'neighborhood': 'Rosales',
    'address': 'Avenue 3 #74a-12',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.684877,
    'longitude': -74.049926,
    'neighborhood': 'Chicó Norte',
    'address': 'Transversal 17 #98',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.66622,
    'longitude': -74.043786,
    'neighborhood': 'El Refugio',
    'address': 'Street 98 con 7',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.66935954667676,
    'longitude': -74.0410900269254,
    'neighborhood': 'Chicó Alto',
    'address': 'Diag 91 no 4A-71',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.69513047472611,
    'longitude': -74.0371607706995,
    'neighborhood': 'Santa Bárbara',
    'address': 'Street 116#11A',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.67918923848767,
    'longitude': -74.0497350613216,
    'neighborhood': 'Chicó Norte',
    'address': 'Avenue 14 con 94',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.69256,
    'longitude': -74.048675,
    'neighborhood': 'Chicó Navarra',
    'address': 'Street 107#22',
    'zone': 'Multicentro'
  },
  {
    'latitude': 4.68410993191174,
    'longitude': -74.0522171207826,
    'neighborhood': 'Chicó Norte III',
    'address': 'Street 97 Avenue 19 A',
    'zone': 'Chicó'
  },
  {
    'latitude': 4.694823,
    'longitude': -74.051369,
    'neighborhood': 'Chicó Navarra',
    'address': 'Avenue 19a #107-34',
    'zone': 'Multicentro'
  },
];

List<String> names = [
  'Jerome',
  'Kiera',
  'Karim',
  'Cordelia',
  'Meir',
  'Ingrid',
  'Yahya',
  'Jaelyn',
  'Billy',
  'Susan',
  'Ernest',
  'Birdie',
  'Alfonso',
  'Kathleen',
  'Jairo',
  'Heavenly',
  'Mayson',
  'Perla',
  'Imran',
  'Christine',
  'Atharv',
  'Emilee',
  'Decker'
      'Guinevere',
  'Lyle',
  'Taliyah',
  'Yael',
  'Billie',
  'Dimitri',
  'Erika',
  'Ignacio',
  'Aarna',
  'Knowledge',
  'India',
  'Kolt',
  'Arely',
  'Merrick',
  'Carla',
  'Agustin',
  'Jewe',
  'Aster',
  'Bayou',
  'Betony',
  'Calix',
  'Clover',
  'Dune',
  'Hawthorn',
  'Lark',
  'Lupin',
  'Marigold',
  'Moss',
  'Ocean',
  'Oleander',
  'Peregrine',
  'Primrose',
  'Rue',
  'Saffron',
  'Soleil',
  'Wolf',
  'Yarrow',
  'Angelou',
  'Audre',
  'Bowie',
  'Coco',
  'Coltrane',
  'Darwin',
  'Django',
  'Ellington',
  'Fitzgerald',
  'Ida',
  'Kahlo',
  'Malala',
  'Matisse',
  'Mercury',
  'Neruda',
  'Orson',
  'Rosalind',
  'Salinger',
  'Tennyson',
  'Wilde',
  'Wilde',
  'Alaska',
  'Anduin',
  'Cuba',
  'Delphi',
  'Everest',
  'Galilee',
  'Havana',
  'Havana',
  'Indiana',
  'Jura',
  'Kitt',
  'Lowena',
  'Maine',
  'Maine',
  'Mali',
  'Nile',
  'Nile',
  'Odessa',
  'Oslo',
  'Sicily',
  'Thessaly',
  'Verona',
];

class RestaurantsProvider {
  static Future<void> patchRestaurants(
    BuildContext context,
  ) async {
    final CollectionReference restaurantsCollection =
        FirebaseFirestore.instance.collection('restaurants');

    final restaurantsQuerySnapshot = await restaurantsCollection.get();
    print('names: ${names.length}');
    print('locations: ${locations.length}');
    print('restaurantsQuerySnapshot: ${restaurantsQuerySnapshot.docs.length}');

    Map<String, dynamic> location;
    QueryDocumentSnapshot restaurant;
    List<QueryDocumentSnapshot> restaurants = restaurantsQuerySnapshot.docs;
    for (int i = 0; i < restaurants.length; i++) {
      restaurant = restaurants[i];
      location = locations[i];
      await restaurantsCollection.doc(restaurant.reference.id).update(
        {
          'latitude': location['latitude'],
          'longitude': location['longitude'],
          'neighborhood': location['neighborhood'],
          'address': location['address'],
          'city': 'Bogota',
          'name': '${names[i]} ${names[i + 50]}',
          'state': 'Cundinamarca',
        },
      );
    }
  }
}
