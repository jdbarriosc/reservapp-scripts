import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:reservapp_scripts/src/model/Booking.dart';
import 'package:reservapp_scripts/src/utils/checkConnectivity.dart';

class BookingsProvider {
  static Future<List<Booking>> deleteBookings(
    BuildContext context,
  ) async {
    final userId = FirebaseAuth.instance.currentUser.email;
    List<Booking> bookings = [];

    if (await checkConnectivity(context)) {
      final CollectionReference bookingsCollection =
          FirebaseFirestore.instance.collection(
        bookingTableName,
      );

      final bookingsQuerySnapshot = await bookingsCollection
          .where(
            'userId',
            isEqualTo: userId,
          )
          .get();

      bookingsQuerySnapshot.docs.forEach((doc) {
        BookingsProvider.deleteBookingById(
          context,
          doc.reference.id,
        );
      });
    }

    return bookings;
  }

  static Future<void> deleteBookingById(
    BuildContext context,
    String bookingId,
  ) async {
    if (await checkConnectivity(context)) {
      final CollectionReference bookingsCollection =
          FirebaseFirestore.instance.collection(bookingTableName);

      return bookingsCollection.doc(bookingId).delete();
    }
    return null;
  }
}
