import 'package:flutter/material.dart';
import 'package:reservapp_scripts/src/providers/RestaurantsProvider.dart';
import 'package:reservapp_scripts/src/providers/UserActionStadisticsProvider.dart';
import 'package:reservapp_scripts/src/providers/UserProvider.dart';

class HomeView extends StatefulWidget {
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  bool hasCompleted = false;

  void runScript() async {
    await RestaurantsProvider.patchRestaurants(context);

    // await BookingsProvider.deleteBookings(context);
    // final userIds = await UserProvider.getUserIds(context);

    // userIds.forEach(
    //   (userId) => UserActionStadisticsProvider.calculateUserActionStatistics(
    //     context,
    //     userId,
    //   ),
    // );

    setState(() {
      hasCompleted = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(
        top: 50.0,
        bottom: 50.0,
      ),
      child: Column(
        children: <Widget>[
          MaterialButton(
            child: Text(
              'Run script',
              style: TextStyle(
                fontSize: 25.0,
                color: Colors.white,
                decoration: TextDecoration.none,
              ),
            ),
            onPressed: runScript,
            color: Colors.red[700],
          ),
          Text(
            hasCompleted ? 'Script runned succesfully.' : '',
            style: TextStyle(
              fontSize: 25.0,
              color: Colors.white,
              decoration: TextDecoration.none,
            ),
          ),
        ],
      ),
    );
  }
}
