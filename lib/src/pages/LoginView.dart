import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:reservapp_scripts/routes.dart';
import 'package:reservapp_scripts/src/utils/SessionContext.dart';
import 'package:reservapp_scripts/src/providers/UserProvider.dart';
import 'package:reservapp_scripts/src/utils/checkConnectivity.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  void initState() {
    super.initState();
  }

  bool progressSpinner = false;
  String emailLocal = '';
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: ModalProgressHUD(
        inAsyncCall: progressSpinner,
        child: Stack(
          children: <Widget>[
            _crearFondo(context),
            _loginForm(context),
          ],
        ),
      ),
    );
  }

  Widget _loginForm(BuildContext context) {
    final bloc = SessionContext.of(context);
    final size = MediaQuery.of(context).size;
    final _auth = FirebaseAuth.instance;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 180.0,
            ),
          ),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 30.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 3.0,
                  offset: Offset(0.0, 5.0),
                  spreadRadius: 3.0,
                ),
              ],
            ),
            child: Column(
              children: <Widget>[
                Text(
                  'Login',
                  style: TextStyle(fontSize: 20.0),
                ),
                SizedBox(height: 60.0),
                _crearEmail(bloc),
                SizedBox(height: 30.0),
                _crearPassword(bloc),
                SizedBox(height: 30.0),
                _crearBoton(
                  bloc,
                  _auth,
                  _scaffoldKey.currentContext,
                )
              ],
            ),
          ),
          FlatButton(
            child: Text('Do not have an account? Create it now'),
            onPressed: () => Navigator.pushReplacementNamed(
              context,
              ROUTES.signup.toString(),
            ),
          ),
          SizedBox(height: 100.0)
        ],
      ),
    );
  }

  Widget _crearEmail(UserProvider bloc) {
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                icon: Icon(
                  Icons.alternate_email,
                  color: Colors.red,
                ),
                hintText: 'example@email.com',
                labelText: 'Email',
                counterText: snapshot.data,
                errorText: snapshot.error),
            onChanged: bloc.changeEmail,
          ),
        );
      },
    );
  }

  Widget _crearPassword(UserProvider bloc) {
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
                icon: Icon(Icons.lock_outline, color: Colors.red),
                labelText: 'Password',
                errorText: snapshot.error),
            onChanged: bloc.changePassword,
          ),
        );
      },
    );
  }

  Widget _crearBoton(
      UserProvider bloc, FirebaseAuth _auth, BuildContext bcontext) {
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
            child: Text('Log In'),
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          elevation: 0.0,
          color: Colors.red,
          textColor: Colors.white,
          onPressed: snapshot.hasData
              ? () async {
                  setState(() {
                    progressSpinner = true;
                  });
                  if (await checkConnectivity(context)) {
                    try {
                      final user = await _auth.signInWithEmailAndPassword(
                          email: bloc.email, password: bloc.password);
                      if (user != null) {
                        await persistEmail(bloc.email);
                        Navigator.pushNamed(
                          bcontext,
                          ROUTES.home.toString(),
                        );
                      }
                      setState(
                        () {
                          progressSpinner = false;
                        },
                      );
                    } on Exception catch (e, s) {
                      await FirebaseCrashlytics.instance.recordError(e, s);
                      setState(
                        () {
                          progressSpinner = false;
                        },
                      );
                      _scaffoldKey.currentState.showSnackBar(
                        SnackBar(
                          content: new Text('Incorrect email or password.'),
                          duration: new Duration(seconds: 5),
                        ),
                      );
                    }
                  }
                }
              : null,
        );
      },
    );
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final fondoModaro = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color>[
            Color.fromRGBO(211, 47, 47, 1.0),
            Color.fromRGBO(195, 45, 45, 1.0)
          ],
        ),
      ),
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.05),
      ),
    );

    return Stack(
      children: <Widget>[
        fondoModaro,
        Positioned(top: 90.0, left: 30.0, child: circulo),
        Positioned(top: -40.0, right: -30.0, child: circulo),
        Positioned(bottom: -50.0, right: -10.0, child: circulo),
        Positioned(bottom: 120.0, right: 20.0, child: circulo),
        Positioned(bottom: -50.0, left: -20.0, child: circulo),
      ],
    );
  }

  Future<void> persistEmail(String email) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('email', email);
  }

  Future<void> getLocalEmail() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      emailLocal = prefs.getString('email');
    });
  }
}
