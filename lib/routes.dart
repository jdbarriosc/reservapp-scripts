import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reservapp_scripts/src/pages/HomeView.dart';
import 'package:reservapp_scripts/src/pages/LoginView.dart';

enum ROUTES {
  login,
  signup,
  home,
  restaurants,
  bookings,
}

final Map<String, Widget Function(BuildContext)> routes = {
  ROUTES.login.toString(): (BuildContext context) => LoginView(),
  ROUTES.home.toString(): (BuildContext context) => Material(
        child: Scaffold(
          backgroundColor: Colors.grey,
          body: HomeView(),
        ),
      )
};
